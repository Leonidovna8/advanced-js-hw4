Теоретичне питання:
1. Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

AJAX - це технологія, яка дозволяє веб-сторінкам "розмовляти" з сервером без перезавантаження сторінки. Вона дозволяє веб-додаткам відправляти та отримувати дані з сервера без перерви у взаємодії з користувачем. JavaScript використовується для реалізації цієї технології, і він допомагає створювати динамічні та відзивчиві веб-додатки. AJAX корисний, оскільки він дозволяє сторінці оновлюватися "на льоту", без перезавантаження, що робить користування веб-додатком більш зручним та швидким.


Завдання:

Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

Технічні вимоги:

1. Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни

2. Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.

3. Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).

4. Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.